import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import { Button } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import { Alert } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';



const Identificacion = ({url, onIdentificar}) => {

    const [error, setError] = useState(false);
    const [visible, setVisible]=useState(true);

    const cerrar = () => {
        setVisible(false);
    }

    const acceder = () => {
        // Recogemos el texto de los campos usuario y contraseña
        let nombre = document.getElementById("nombre").value;
        let clave = document.getElementById("clave").value;

        var usuario = {
            nombre:nombre,
            clave:clave,
            nombreCompleto: '',
            id: ''
        }

        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ accion:'acceder', usuario: usuario.nombre, clave: usuario.clave })
        };

        // process.env.PUBLIC_URL --> http://localhots:3000 url de npm

        fetch(url, requestOptions).then(response => response.json().then(
            data => ({
                data: data,
                status: response.status
            })).then(res => {
                if (res.data.respuesta) {
                    cerrar();
                    usuario.id = res.data.usuario.id;
                    onIdentificar(usuario);
                } else {
                    setError(true);
                }
            }));

    }

    return (
        <Modal
            show={visible}
            onHide={cerrar}
            backdrop="static"
            keyboard={false}
        >
            <Modal.Header closeButton>
                <Modal.Title>Identificación</Modal.Title>
            </Modal.Header>
            {
                error && <Alert key="danger" variant="danger">
                            Usuario incorrecto
                        </Alert>
            }
            <Modal.Body>
                <Form>
                    <Form.Group className="mb-3" controlId="nombre">
                        <Form.Label>Usuario</Form.Label>
                        <Form.Control type="text"  placeholder="nombre de usuario" />
                        <Form.Text className="text-muted">
                            No compartiremos tu nombre de usuario con nadie
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="clave">
                        <Form.Label>Contraseña</Form.Label>
                        <Form.Control type="password" placeholder="contraseña" />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={cerrar}>
                    Cerrar
                </Button>
                <Button variant="primary" onClick={acceder}>
                    Acceder
                </Button>
            </Modal.Footer>
        </Modal>
    );
}

export default Identificacion;