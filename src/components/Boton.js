import '../pages/Pages.css'
import { Button } from 'semantic-ui-react'

const Boton = ({ rotuloBot, onPulsar, colorBot, sizeBot, floatedBot, iconBot }) => {

    return (
        <Button 
            color={colorBot}
            size={sizeBot}
            floated={floatedBot}
            onClick={onPulsar}
            icon={iconBot}
            >
            {rotuloBot}
        </Button>

    )
}

export default Boton;