import React from 'react';
import { Drawer } from 'antd';
import { useState } from 'react';

export default function Cajon ({titulo, contenido, abierto, extra}) {
  const [open, setOpen] = useState(true);
  const onClose = () => {
    setOpen(false);
  };


  return (
    <>
        <Drawer
            title={titulo}
            placement='left'
            extra={extra}
            closable={false}
            onClose={onClose}
            open={abierto}
        >
            {contenido}
        </Drawer>
    </>
  );
};

