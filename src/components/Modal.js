import '../pages/Pages.css'
import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import ReactDOM from 'react-dom';


const ModalContacto = ({ visible, imagenModal, rotulo, cerrarModal }) => {
  
    const [show, setShow] = useState(true);
    const contenedor=document.querySelector("#contenedorModal");
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        visible &&
        ReactDOM.createPortal(
          <>
            <Modal
              show={show}
              onHide={handleClose}
              backdrop="static"
              keyboard={false}
            >
              <Modal.Header closeButton>
                <Modal.Title>{rotulo}</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                  <img style={{height: '12rem', width: '12rem', marginLeft: '20px'}} src={process.env.PUBLIC_URL + `/${imagenModal}`} className="card-img-top" alt={rotulo} />
                  <div style={{float: 'right', marginRight: '110px', marginTop: '-160px'}}>
                    <h5 style={{fontWeight: 'bold'}}>Nombre y apellidos</h5>
                    <p>Pablo Fernández</p>
                    <h5 style={{fontWeight: 'bold'}}>Nombre de usuario</h5>
                    <p>@pabfer23</p>
                    <h5 style={{fontWeight: 'bold'}}>Teléfono</h5>
                    <p>656 41 22 34</p>
                    <h5 style={{fontWeight: 'bold'}}>Biografía</h5>
                    <p>Hola!</p>
                  </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="primary" onClick={cerrarModal} >
                  Cerrar
                </Button>
              </Modal.Footer>
            </Modal>
           </> , contenedor)
          
        )
    
};
  
export default ModalContacto;