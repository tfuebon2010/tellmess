import '../pages/Pages.css'
import { Message } from 'semantic-ui-react'
import { useEffect, useState } from 'react'

const Mensaje = ({ mensajes, usuario }) => {
  if (mensajes.length > 0) {
    return (
      <>
        {mensajes && mensajes.map((mensajes) =>
          <>
            <div>
              <Message
                key={mensajes.id}
                className={mensajes.id_remitente === usuario.id ? "enviados" : "recibidos"}
                content={mensajes.mensaje}
                compact floating size='large'
                color={mensajes.id_remitente === usuario.id ? "blue" : "green"}
              />
            </div>
            <div style={{ clear: 'both' }}></div>
          </>
        )}
      </>
    )
  } else {
    return <p>Cargando...</p>
  }
}

export default Mensaje
