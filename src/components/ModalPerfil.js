import React, { Fragment, useState, useEffect } from 'react'
import '../pages/Pages.css'
import Contacto from './Contacto';
import Cajon from './Cajon';
import { Space, Button } from 'antd';



const ModalPerfil = ({setSecondOpen}) => {

    const [perfil, setPerfil] = useState([])
    const [open, setOpen] = useState(true);
    const onClose = () => {
      setOpen(false);
    };
    
    useEffect(() => {
        fetch(process.env.PUBLIC_URL+'/perfiles.json', {
          method:'GET',
          mode:'no-cors',
          headers: {
            'content-type':"application/json"
          }
        })
          .then((res) => res.json())
          .then((perfil) => {
            setPerfil(perfil);
            console.log(perfil);
        })
    }, []);
    


    if (Object.keys(perfil).length !== 0){
 
        return (
              
            <Cajon 
              abierto={open}
              titulo='Perfiles'
              extra={
                <Space>
                  <Button type="primary" onClick={onClose}>
                    Salir
                  </Button>
                </Space>
              }
              contenido={
                <>
                  {perfil.map((item) => 
                      <Contacto 
                          key={item.id} 
                          foto={item.imagen} 
                          nombre={item.nombre} 
                          contenido={item.usuario} 
                          alPulsar={() => {
                            setSecondOpen(true)
                          }} 
                      />
                  )}
                </>
              }
            />
        )
    }
}
    
  



export default ModalPerfil;