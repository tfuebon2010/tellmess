import '../pages/Pages.css'
import { Button } from 'semantic-ui-react'

const BotonMenu = ({ rotuloBot, onPulsar, sizeBot, floatedBot, idBot, imagenBot }) => {

    return (
        <Button 
            size={sizeBot}
            floated={floatedBot}
            onClick={onPulsar}
            id={idBot}
            className='opciones'
        >   
            <img src={process.env.PUBLIC_URL + `/${imagenBot}`} />
            <br></br>
            {rotuloBot}
        </Button>

    )
}

export default BotonMenu;