import '../pages/Pages.css'
import { Card, Feed } from 'semantic-ui-react'

const Contacto = ({ foto, nombre, contenido, alPulsar }) => {
    return (
        <Card onClick={alPulsar}>
            <Card.Content>
            <Feed>
                <Feed.Event>
                <Feed.Label image={process.env.PUBLIC_URL + `/${foto}`} />
                <Feed.Content>
                    <Feed.Summary>
                        <a>{nombre}</a>
                    </Feed.Summary>
                    <Feed.Date content={contenido} />
                </Feed.Content>
                </Feed.Event>
            </Feed>
            </Card.Content>
        </Card>
    )
}

export default Contacto;