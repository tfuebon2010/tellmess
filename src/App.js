import './App.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'semantic-ui-css/semantic.min.css'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import { useEffect, useState } from 'react';
import Chat from './pages/Chat'
import Contactos from './pages/Contactos'
import Ajustes from './pages/Ajustes'
import MenuPrincipal from './pages/Menu';
import Perfiles from './pages/Perfiles'
import Identificacion from './components/Identificacion';


export default function App() {

  const [url, setUrl] = useState(null);
  const [isVisible, setChatVisible] = useState(false);
  const [usuario, setUsuario] = useState(null)

  function identificar(usuario) {
    setUsuario(usuario);
    setChatVisible(true);
  }

  useEffect(() => {

    fetch(process.env.PUBLIC_URL+"/conexion.json")
    .then (response => response.json())
    .then (datos => {
      setUrl(datos.url);
    })
  }, [])

  return (
    <div id='app'>
      <BrowserRouter>
        <MenuPrincipal />

        {url &&
          <Identificacion url={url} onIdentificar={function(usuario) {
            identificar(usuario);
          }}/>
        }
        {url && isVisible && usuario &&
          <Chat url={url} usuario={usuario}/>
        }
        <Routes>
          <Route path='/Contactos' element={<Contactos />} />
          <Route path='/Ajustes' element={<Ajustes />} />
          <Route path='/Perfiles' element={<Perfiles />} />
        </Routes>
      </BrowserRouter>
    </div>
  )
};