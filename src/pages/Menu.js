//import { Link, Outlet } from "react-router-dom";
import './Pages.css'
import { ChakraProvider } from '@chakra-ui/react'
import { Menu, MenuButton, IconButton, MenuList, MenuItem } from '@chakra-ui/react'
import Hamburger from 'hamburger-react'
import {LinkContainer} from 'react-router-bootstrap';
import BotonMenu from '../components/BotonMenu';


function MenuPrincipal({abrirPerfiles}) {

    return (
    <ChakraProvider>
        <Menu>
            <MenuButton
                as={IconButton}
                aria-label='Options'
                icon={<Hamburger />}
                variant='outline'
            />
            <MenuList className='menuList'>
                <LinkContainer to="/Contactos">    
                    <MenuItem>
                        <BotonMenu
                            idBot="o1"
                            rotuloBot="Contactos"
                            imagenBot="/imgs/contact.png"    
                        />
                    </MenuItem>
                </LinkContainer>
                <LinkContainer to="/Ajustes">    
                    <MenuItem>
                        <BotonMenu
                            idBot="o2"
                            rotuloBot="Ajustes"
                            imagenBot="/imgs/setting.png"   
                        />
                    </MenuItem>
                </LinkContainer>
                <LinkContainer to="/Perfiles">    
                    <MenuItem>
                        <BotonMenu
                            idBot="o3"
                            rotuloBot="Perfil"
                            imagenBot="/imgs/perfil.png"
                            onPulsar={abrirPerfiles}    
                        />
                    </MenuItem>
                </LinkContainer>
            </MenuList>
        </Menu>
        
    </ChakraProvider>

    )
}


export default MenuPrincipal;