import './Pages.css'
import Mensaje from '../components/Mensaje';
import { Input } from 'semantic-ui-react'
import { Button } from 'semantic-ui-react'
import Contacto from '../components/Contacto';
import React, { useState, useEffect } from 'react';
import Modal from '../components/Modal';
import ModalContacto from '../components/Modal';

const Chat = ({url, usuario}) => {

    const [mensajes, addMensaje] = useState()
    const [modal, setModal] = useState(false)

    const cabecera = {

        method: 'POST',
        headers: { 'Content-Type': 'application/json'},
        body: JSON.stringify({ accion:'leermensajes', usuario: usuario.nombre, clave: usuario.clave })
    }

    useEffect(() => {
        fetch(url, cabecera)
        .then(response => response.json())
        .then(datos => {
            addMensaje(datos);
            //setCuestionarios(datos);
        });
    }, []);

    const agregarMensaje = mensaje => {
      addMensaje([...mensajes, mensaje]);
    };
  
    const enviarMensaje = () => {
      const texto = document.getElementById('texto').value;
      const mensaje = { 
        id_remitente: usuario.id, 
        id_destinatario: 4, 
        mensaje: texto 
      };
      cabecera.body = JSON.stringify({ accion: 'enviarmensaje', ...mensaje });
      fetch(url, cabecera)
        .then(response => response.json())
        .then(datos => {
          agregarMensaje(mensaje);
          console.log(datos);
        });
      document.getElementById('texto').value = '';
    };


    return (
      <div className="chat" >
          <Contacto
            className='contactoChat'
            nombre='Pablo Fernández'
            foto='/imgs/p4.png'
            contenido='Recientemente'
            alPulsar={() => {
              setModal(true)
              console.log(modal)
            }}/>
            <ModalContacto
              visible={modal}
              imagenModal='/imgs/p4.png'
              rotulo='Detalles del contacto'
              cerrarModal={() => {
                setModal(false)}
              }
            />
          {mensajes &&
              <Mensaje mensajes={mensajes} usuario={usuario}/>
          }
          <br></br>
          <div id='inputs' >
            <Input fluid size='huge' placeholder='Escriba su texto...' id='texto' style={{ float: 'left', width: '92%' }} />
            <Button
              style={{ float: 'right', width: '4.25%' }}
              circular
              icon='send'
              color='blue'
              size='huge'
              floated='right'
              onClick={() => {
               
                enviarMensaje()
              }}
            />

          </div>
      </div>
  )
}
/*
style={{ backgroundImage: `url(${process.env.PUBLIC_URL + '/imgs/fondo1.png'})` }}

onClick={() => {
              let texto = document.getElementById('texto').value;
              alEnviar={texto}
              addMensaje([...mensajesTotales, texto])

            }}  */

/*function Chat() {
    const [value, setValue] = React.useState('');
    return (
      <TextInput
        placeholder="type here"
        value={value}
        onChange={event => setValue(event.target.value)}
      />
    );
  }*/


export default Chat;
