import './Pages.css'
import React, { useEffect, useState } from 'react';
import Contacto from '../components/Contacto';
import Cajon from '../components/Cajon';
import { Space, Button } from 'antd';


function Contactos() {
  
    const [contacto, addContacto] = useState([])
    const [open, setOpen] = useState(true);
    const onClose = () => {
      setOpen(false);
    };


    useEffect(() => {
      fetch(process.env.PUBLIC_URL+'/contactos.json', {
        method:'GET',
        mode:'no-cors',
        headers: {
          'content-type':"application/json"
        }
      })
        .then((res) => res.json())
        .then((contacto) => {
          addContacto(contacto);
          console.log(contacto);
        })
    }, []);

    if (Object.keys(contacto).length !== 0){
      return (
        <Cajon
          abierto={open}
          titulo='Contactos'
          extra={
            <Space>
              <Button type="primary" onClick={onClose}>
                Salir
              </Button>
            </Space>
          }
          contenido={
            contacto.map((item) => 
              <Contacto key={item.id} nombre={item.nombre} foto={item.imagen} contenido={item.conexion} />
            )}
        />
      )
    }
}


export default Contactos;