<?php

class Mensaje {

    private $id;
    private $id_remitente;
    private $id_destinatario;
    private $mensaje;

    public function __construct($datos) {
        if ($datos != null) {
            $this->id_remitente=$datos["id_remitente"];
            $this->id_destinatario=$datos["id_destinatario"];
            $this->mensaje = $datos["mensaje"];
        } else{
            $this->id_remitente="";
            $this->id_destinatario="";
            $this->mensaje="";
        }
    }

    public function getId() {
        return $this->id;
    }

    public function setId($i) {
        $this->id;
    }

    public function getRemitente() {
        return $this->id_remitente;

    }

    public function setRemitente($r) {
        $this->id_remitente=$r;
    }

    public function getDestinatario() {
        return $this->id_destinatario;
    }

    public function setDestinatario($d) {
        $this->id_destinatario=$d;
    }

    public function getMensaje() {
        return $this->mensaje;
    }

    public function setMensaje($m) {
        $this->mensaje=$m;
    }

    public function toArray() {
        return (get_object_vars($this));
    }
}

?>
