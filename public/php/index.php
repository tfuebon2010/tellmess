<?php
require_once("./cabeceras.php");
require_once("./Usuario.php");
require_once("./UsuarioDB.php");



/* Recogida de datos,
  file_get_contents("php://input") es similar a $_REQUEST, $_POST

  Con json_decode obtenemos un objeto json que se manipula
  como un array asociativo
*/


/*
Ejemplo de eco:
Directamente
$json = array('respuesta' => $datos["usuario"]["nombre"]);
*/
// Descomponiendlo en arrays asociativos
$datos = json_decode(file_get_contents("php://input"), true);

$accion = $datos["accion"];
//$accion=$_REQUEST["accion"];
//$accion = "acceder";

$usuarioDB = new UsuarioDB();
//var_dump($usuarioDB);

switch ($accion) {
  case "acceder":
      $usuario= new Usuario($datos);
      //$usuario = "prueba";
      $exito = $usuarioDB->comprueba($usuario);
      if ($exito) {
          $usuarioDB->leerMensajes($usuario);
      }
      $respuesta = array("respuesta" => $exito, "usuario" => $usuario->toArray());
      echo json_encode($respuesta);
      break;

      /*case "leerdestinatarios":
        $usuario=new Usuario($datos["usuario"]);
        $destinatarios = $usuarioDB->leerDestinatarios($usuario);
        $a = array();
        foreach ($destinatarios as $destinatario) {
          array_push($a, $destinatario->getObjectVars());

        }
        echo json_encode($a);
        break;
    */

      case "enviarmensaje":
        $mensaje=new Mensaje($datos);
        $exito=$usuarioDB->enviarMensaje($mensaje);
        $json = array('exito' => $exito);
        echo json_encode($json);
        break;

      case "leermensajes":
        $usuario = new Usuario($datos);
        $exito = $usuarioDB->comprueba($usuario);
        if ($exito) {
            $usuarioDB->leerMensajes($usuario);
        }
        $usuarioDB->leerMensajes($usuario);
        $a = $usuario->getChat()->toArray();
        echo json_encode($a);
        break;

      /*case "borrarmensaje":
          $id=$datos["id"];
          $exito = $usuarioDB->borrarMensaje($id);
          $json = array('exito' => $exito);
          echo json_encode($json);
          break;*/

      default:
        $error = array ('error' => 'Tienes que pasarle una accion');
        echo json_encode($error);
        break;

}

$usuarioDB->cerrar();


?>
