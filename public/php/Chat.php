<?php
require "Mensaje.php";

class Chat {

    private $mensajes;
    
    /* Creamos un array que albegará todos los objetos 
    mensaje (entrantes y salientes) del usuario */
    public function __construct() {
        $this->mensajes=array();        
    }

    // Obtenemos el array de mensajes
    public function getMensajes() {
        return $this->mensajes;
    }

    // Añadimos un objeto mensaje $m al array de mensajes
    public function addMensaje($m) {
        array_push($this->mensajes, $m);
    }

    // Obtenemos los mensajes entrantes del usuario
    public function getMensajesEntrantes($login) {
        // Creamos un array para los mensajes entrantes
        $entrantes=array();
        // Recorremos el array
        foreach ($this->mensajes as $mensaje) {
            // Nos quedamos con los mensajes entrantes
            // Que son aquellos cuyo destinatario es el mismo usuario
            // es decir, coinciden con el login del usuario
            if ($mensaje->getDestinatario() == $login) {
                // Guardamos en el array el mensaje
                array_push($entrantes, $mensaje);
            }
        }
        // Devolvemos el array
        return $entrantes;
    }

    // Obtenemos los mensajes salientes del usuario
    public function getMensajesSalientes($login) {
        // Creamos un array para los mensajes salientes
        $salientes=array();
        // Recorremos el array
        foreach ($this->mensajes as $mensaje) {
            // Nos quedamos con los mensajes salientes
            // Que son aquellos cuyo remitente es el mismo usuario
            // es decir, coinciden con el login del usuario
            if ($mensaje->getRemitente() == $login) {
                // Guardamos en el array el mensaje
                array_push($salientes, $mensaje);
            }
        }
        // Devolvemos el array
        return $salientes;
    }

    // Elimina el array mensajes y lo volvemos a crear
    public function borrarMensajes() {
        unset($this->mensajes);
        $this->mensajes=array();
    }

    public function toArray() {

        $a = array();

        foreach ($this->mensajes as $mensaje) {
            array_push($a, $mensaje->toArray());
        }
        return $a;


    }

}


?>