<?php
require "Chat.php";

/* Un usuario es un objeto que representa a una tupla en la base de
datos usuarios

También permite crear objetos con datos obtenidos desde un formulario */

class Usuario {

    // Sus propiedades o atributos
    private $id;
    private $usuario;
    private $clave;
    private $nombreCompleto;
    private $telefono;
    private $biografia;

    /* El buzón es un objeto cuya representación es un array 
     de objetos mensaje */
    private $chat;

    /* El método constructor recibe un array asociativo desde un formulario
    o desde una tupla de la base de datos y crea un objeto con los datos del array*/

    public function __construct($datos)  
    {
        
        $this->usuario = $datos["usuario"];
        $this->clave = $datos["clave"];

        if (!isset($datos["id"])){
            $this->id = null;
            $this->nombreCompleto = null;
            $this->telefono = null;
            $this->biografia = null;
            
        
        }else{
            $this->id = $datos["id"];
            $this->nombreCompleto = $datos["nombreCompleto"];
            $this->telefono = $datos["telefono"];
            $this->biografia = $datos["biografia"];
        }
        $this->chat=new Chat();        
    }

    // Métodos set y get para cada propiedad

    public function setId($i) {
        $this->id=$i;
    }

    public function getId() {
        return $this->id;
    }

    /*public function setNombre($n) {
        $this->nombre=$n;
    }

    public function getNombre() {
        return $this->nombre;
    }*/

    public function setUsuario($u) {
        $this->usuario=$u;
    }

    public function getUsuario() {
        return $this->usuario;
    }


    public function setNombreCompleto($nc) {
        $this->nombreCompleto=$nc;
    }

    public function getNombreCompleto() {
        return $this->nombreCompleto;
    }

    public function setClave($c) {
        $this->clave=$c;
    }

    public function getClave() {
        return $this->clave;
    }

    public function setTelefono($t) {
        $this->telefono=$t;
    }

    public function getTelefono() {
        return $this->telefono;
    }

    public function setImagen($i) {
        $this->imagen=$i;
    }

    public function getImagen() {
        return $this->imagen;
    }

    public function setBiografia($b) {
        $this->biografia=$b;
    }

    public function getBiografia() {
        return $this->biografia;
    }

    
    // Este método devuelve el buzón del usuario
    public function getChat() {
        return $this->chat;
    }

    // Método que convierte un objeto en un array
    public function toArray() {

        $a = get_object_vars($this);
        $a["chat"]=$this->chat->toArray();

        return ($a);

    }

    public function getObjectVars() {

        return (get_object_vars($this));
    }

}
?>