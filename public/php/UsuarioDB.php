<?php

class UsuarioDB {

    private $conexion;
    private $servidor;
    private $usuario;
    private $clave;
    private $basedatos;

    // Creamos la conexión con la base de datos
    // Si hay error detenemos las aplicación
    public function __construct() {

        // Leemos el fichero de configuración
        $credenciales=parse_ini_file("conexion.ini");

        $this->servidor=$credenciales["servidor"];
        $this->usuario=$credenciales["usuario"];
        $this->clave=$credenciales["clave"];
        $this->basedatos=$credenciales["basedatos"];

        $this->conexion = new mysqli($this->servidor, $this->usuario, $this->clave, $this->basedatos);
        if ($this->conexion->connect_error == true) {
            die("Error de conexion".$this->conexion->connect_error.":".$this->conexion->connect_errno);
        }
        //echo "Conexión exitosa";
    }

    /* Comprobamos mediante una consulta que las conexion
    del usuario coinciden con las de algún usuario o error */
    public function comprueba($usuario) {

        // Suponemos que no habrá éxito
        $exito=false;
        // Obtenemos el login y la contraseña
        $login=$usuario->getUsuario();
        $clave=$usuario->getClave();
        // Escribimos la consulta
        $sql = "select * from usuarios where usuario = '$login' and clave = '$clave'";
        // Ejecutamos la consulta
        $resultado=$this->conexion->query($sql);
        // Si devuelve más de una tupla
        if ($resultado->num_rows > 0) {
            $row = $resultado->fetch_assoc();
            $usuario->setId($row['id']);
            $exito=true;
        }
        // Devolvemos el valor de éxito
        return $exito;
    }


    // Lee todos los mensajes de un usuario
    public function leerMensajes($usuario) {
        // Borramos los mensajes previos
        $usuario->getChat()->borrarMensajes();
        // Obtenemos el logín (nombre) del usuario
        $login=$usuario->getId();
        // Escribimos la consulta
        $sql="select * from mensajes where id_destinatario='$login' or id_remitente='$login'";
        // Ejecutamos la consulta
        $resultado=$this->conexion->query($sql);
        // Cogemos la primera tupla
        $tupla = $resultado->fetch_assoc();
        // Si hay tupla
        while ($tupla != null) {
            // Creamos un objeto mensaje con los datos de la consulta
            $mensaje=new Mensaje($tupla);
            // Obtenemos el buzón del usuario (todos los mensajes)
            $chat=$usuario->getChat();
            // Añadimos un mensaje al buzón
            $chat->addMensaje($mensaje);
            // Cogemos una nueva tupla
            $tupla = $resultado->fetch_assoc();
        }
    }

    // Leemos todos los usuarios de la tabla salvo al que pasamos como
    // parámetro
    public function leerDestinatarios($usuario) {

        // Creamos un array vacio
        $destinatarios=array();
        // Obtenemos el login del usuario
        $login=$usuario->getNombre();
        // Escribimos la consulta
        $sql="select * from usuarios where usuario!='$login'";
        // Ejecutamos la consulta que devolverá un cursor
        $cursor=$this->conexion->query($sql);
        // Cogemos la primera tupla
        $tupla=$cursor->fetch_assoc();
        // Si existe al menos un tupla
        while ($tupla != null) {
            // Creamos un nuevo objeto usuario con los datos de la tupla
            $nuevoUsuario=new Usuario($tupla);
            // Guardamos en el array destinatarios el objeto nuevoUsuario
            array_push($destinatarios, $nuevoUsuario);
            // Cogemos la siguiente tupla
            $tupla=$cursor->fetch_assoc();
        }
        // Devolvemos el array de destintarios
        return $destinatarios;

    }

    // Graba un mensaje en la tabla mensajes
    public function enviarMensaje($mensaje) {

        /* Obtenemos en variables independientes cada propiedad
        del mensaje */
        $r=$mensaje->getRemitente();
        $d=$mensaje->getDestinatario();
        $c=$mensaje->getMensaje();
        // Escribimos la consulta
        $sql="insert into mensajes(id_remitente,id_destinatario,mensaje) values('$r','$d','$c')";
        // Ejecutamos la consulta
        $exito=$this->conexion->query($sql);
        // Devolvemos el valor de éxito o error de la consulta
        return $exito;

    }

    /* Borra un mensaje de la tabla mensajes */
    // Le pasamos el id de un mensaje
    public function borrarMensaje($id) {
        // Escribimos la consulta
        $sql="delete from mensajes where id=$id";
        // Ejecutamos la consulta
        $exito=$this->conexion->query($sql);
        // Devolvemos el estado de acierto o error del consulta
        return $exito;
    }

    // Cierra la conexión con la base de datos
    public function cerrar() {
        $this->conexion->close();
    }

}
?>














